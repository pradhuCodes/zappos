package intern.ilovezappos;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.daprlabs.cardstack.SwipeDeck;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ZapposActivity extends AppCompatActivity implements Callback<ZapposResponse> {

    private SwipeDeck cardStack;
    private int SUCCESS=200;
    ArrayList<ZapposDataObject> zapposDataObjects = new ArrayList<>();
    SwipeDeckAdapter adapter;
    TextView count;
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zappos);
        initializeCards();
        count = (TextView)findViewById(R.id.count);
        counter = 0;
    }

    @Override
    public void onResponse(Call<ZapposResponse> call, Response<ZapposResponse> res) {
        ZapposResponse response = res.body();
        if(response.getStatusCode()==SUCCESS) {
            zapposDataObjects = response.getResults();
            if(adapter!=null) {
                adapter.updateData(zapposDataObjects);
            }
        } else {
            Toast.makeText(this,"No results found",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<ZapposResponse> call, Throwable t) {

    }

    void initializeCards() {
        cardStack = (SwipeDeck) findViewById(R.id.swipe_deck);
        adapter = new SwipeDeckAdapter(zapposDataObjects, this);
        cardStack.setAdapter(adapter);
        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {

            }

            @Override
            public void cardSwipedRight(int position) {
                counter++;
                if(count!=null) {
                    count.setText(""+counter);
                }
            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_view_menu_item, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                zapposDataObjects.clear();
                callZapposAPI(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    void callZapposAPI(String query) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.zappos.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ZapposAPI zapposAPI = retrofit.create(ZapposAPI.class);
        Call<ZapposResponse> call = zapposAPI.loadResults(query);
        call.enqueue(this);
    }
}
