package intern.ilovezappos;

import java.util.ArrayList;

/**
 * Created by pradhumanswami on 1/27/17.
 */

public class ZapposResponse {
    public String getOriginalTerm() {
        return originalTerm;
    }

    public void setOriginalTerm(String originalTerm) {
        this.originalTerm = originalTerm;
    }

    public int getCurrentResultCount() {
        return currentResultCount;
    }

    public void setCurrentResultCount(int currentResultCount) {
        this.currentResultCount = currentResultCount;
    }

    public int getTotalResultCount() {
        return totalResultCount;
    }

    public void setTotalResultCount(int totalResultCount) {
        this.totalResultCount = totalResultCount;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public ArrayList<ZapposDataObject> getResults() {
        return results;
    }

    public void setResults(ArrayList<ZapposDataObject> results) {
        this.results = results;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    String originalTerm;
    int currentResultCount;
    int totalResultCount;
    String term;
    ArrayList<ZapposDataObject> results = new ArrayList<ZapposDataObject>();
    int statusCode;
}
