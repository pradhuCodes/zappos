package intern.ilovezappos;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import intern.ilovezappos.databinding.CardViewZapposItemBinding;

/**
 * Created by pradhumanswami on 1/27/17.
 */

public class SwipeDeckAdapter extends BaseAdapter {

    private List<ZapposDataObject> data;
    private Context context;

    public SwipeDeckAdapter(List<ZapposDataObject> data, Context context) {
        this.data = data;
        this.context = context;
    }

    public void updateData(List<ZapposDataObject> data) {
        this.data = data;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        CardViewZapposItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.card_view_zappos_item, parent, false);
        binding.setZapposObject(data.get(position));
        return binding.getRoot();
    }
}
