package intern.ilovezappos;

/**
 * Created by pradhumanswami on 1/27/17.
 */
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
public interface ZapposAPI {
    @GET("/Search?key=b743e26728e16b81da139182bb2094357c31d331")
    Call<ZapposResponse> loadResults(@Query("term") String tags);
}
